import React from 'react';
import Dashboard from './components/Dashboard'

class App extends React.PureComponent {
    render() {
        return (
            <div className="App">
                <Dashboard />
            </div>
        );
    }
}

export default App;
