import './index.css'
import React from 'react'
import { connect } from 'react-redux'
import { fetchData } from '../../actions'
import getRandomNum from '../../util/random-num'
import DataBar from '../DataBar'

class Dashboard extends React.Component {
    constructor( props ) {
        super( props )
        this.state = {
            currentSort: null,
            sortDir: false
        }
        this.sortByValue = this.sortByValue.bind( this )
    }
    componentDidMount() {
        this.props.fetchData()
    }

    sortByValue() {
        this.props.data.items.sort( ( a, b ) => {
            return this.state.sortDir ? b.value - a.value : a.value - b.value
        } )
        this.setState( { sortDir: !this.state.sortDir } )
    }

    render() {
        return (
            <div className="Dashboard">
                {this.props.data && this.props.data.columns ?
                    <table className="data-table" cellSpacing="0" cellPadding="0">
                        <thead>
                            <tr>
                                {this.props.data.columns.map( c => <th key={`column-${getRandomNum()}`}>{c}</th> )}
                                <th>Валюта</th>
                                <th className="pointer" onClick={this.sortByValue}>Отклонение от плана, п.п.</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.data.items.map( el => {
                                return ( <tr key={`table-row-${getRandomNum()}`}>
                                    {el.axisData.map( ad => ( <td key={`axis-data-${getRandomNum()}`}>
                                        <div className="cut-long-lines" title={ad}>{ad}</div>
                                    </td> ) )}
                                    <td><div className="cut-long-lines" title={el.measure}>{el.measure}</div></td>
                                    <td>
                                        <DataBar value={el.value > 0 ? 0 : el.value} max={this.props.data.maxValue} />
                                        <div className="data-value">{( el.value > 0 ? '+ ' : '- ' ) + Math.abs( el.value )} п.п.</div>
                                        <DataBar value={el.value > 0 ? el.value : 0} max={this.props.data.maxValue} />
                                    </td>
                                </tr> )
                            } )}

                        </tbody>
                    </table>
                    : null}
                {this.props.isLoading ? <h3 className="text-center">Загрузка...</h3> : null}
                {this.props.fetchError ? <h3 className="text-center">Ошибка :( Причина: {this.props.fetchErrorReason}</h3> : null}
            </div>
        )
    }
}
Dashboard = connect(
    ( { dashboard } ) => ( { ...dashboard } ),
    { fetchData }
)( Dashboard )

export default Dashboard
