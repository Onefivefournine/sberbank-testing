import './index.css'
import React from 'react'

export default class DataBar extends React.PureComponent {
    constructor( props ) {
        super( props )
        this.state = {
            width: 0
        }
    }

    componentDidMount() {
        setTimeout( () => {
            this.setState( { width: Math.abs( this.props.value ) * 190 / Math.abs( this.props.max ) } )
        }, 0 );
    }

    render() {
        return <div className="data-bar">
            <div className={`data-bar__field  data-bar--${this.props.value > 0 ? 'success' : ( this.props.value === 0 ? 'hidden' : 'fail' )}`}>
                {this.props.value ? <div className="data-bar__value"
                    style={( { 'width': ( this.state.width || 0 ) + 'px' } )}
                ></div> : null}
            </div>
        </div>
    }
}