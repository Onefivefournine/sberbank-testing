import dashboard from './dashboard'
import { combineReducers } from 'redux'

export default combineReducers({ dashboard })
