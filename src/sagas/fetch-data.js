import { call, put, takeLatest } from 'redux-saga/effects';
import Api from '../api';

export function* fetchData() {
  try {
    const data = yield call( Api.fetchUrl.bind( Api ), '/data' );
    let maxValue = -Infinity
    const mappedData = data.fa.fa_data.r.map( el => {
      if ( el.fDeltaPlan > maxValue ) maxValue = el.fDeltaPlan
      return {
        axisData: el.axis.r.map( a => a.sName_RU ),
        value: Math.round( el.fDeltaPlan ),
        measure: el.sMeasDelta_RU
      }
    } )

    yield put( {
      type: 'DATA_RECEIVED', data: {
        columns: data.fa.fa_data.axis.r.map( a => a.sAxisName ),
        items: mappedData,
        maxValue
      }
    } );
  } catch ( e ) {
    yield put( { type: 'FETCH_FAILED', reason: e.message } );
  }
}

export function* fetchDataWatcher() {
  yield takeLatest( 'FETCH_DATA', fetchData );
}
